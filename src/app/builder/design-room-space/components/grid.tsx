'use client'
import React, { useState, useEffect } from "react";
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from "@/registry/ui/select";
import { Button } from "@/registry/ui/button";
import {useSearchParams} from "next/navigation";
import {CardFooter} from "@/registry/ui/card";
import Link from "next/link";

const Grid = () => {
    const searchParams = useSearchParams();
    const name = searchParams.get('name');
    const superficie = searchParams.get('superficie');
    const width = 500;
    const height = 500;
    const [cliquedSquares, setCliquedSquares] = useState<string[]>([]);
    const [selectedSquares, setSelectedSquares] = useState<string[]>([]);
    const [isMouseDown, setIsMouseDown] = useState<boolean>(false);
    const [gridSize, setGridSize ] = useState<number>(10);
    const [grid, setGrid] = useState<JSX.Element[]>([]);

    const handleSquareClick = (squareId: string) => {
        if (cliquedSquares.includes(squareId)) {
            setCliquedSquares(cliquedSquares.filter((id) => id !== squareId));
        } else {
            setCliquedSquares([...cliquedSquares, squareId]);
        }
    };

    const coordinatesList = cliquedSquares.map((squareId) => {
        const [x, y] = squareId.split('-').map(Number);
        return [x, y];
    });
    const coordinatesListStr = btoa(JSON.stringify(coordinatesList)); // base64


    const handleMouseDown = (squareId: string) => {
        setIsMouseDown(true);
        setSelectedSquares([...selectedSquares, squareId]);
        handleMouseEnter(squareId);
    };

    const handleMouseUp = () => {
        setIsMouseDown(false);
        fillSpace();
    };

    const fillSpace = () => {
        if (selectedSquares.length > 0) {

            const firstSquare = selectedSquares[0];
            let lastSquare = firstSquare;

            const [firstX, firstY] = firstSquare.split('-').map(Number);
            let lastX = firstX;
            let lastY = firstY;

            for (let currentSquare = 1; currentSquare < selectedSquares.length; currentSquare++) {
                const [xTmp, yTmp] = selectedSquares[currentSquare].split('-').map(Number);


                const distance = Math.sqrt(Math.pow(xTmp - firstX, 2) + Math.pow(yTmp - firstY, 2));

                if (distance > Math.sqrt(Math.pow(lastX - firstX, 2) + Math.pow(lastY - firstY, 2))) {
                    lastSquare = selectedSquares[currentSquare];
                    lastX = xTmp;
                    lastY = yTmp;
                }
            }

            const minX = Math.min(firstX, lastX);
            const maxX = Math.max(firstX, lastX);
            const minY = Math.min(firstY, lastY);
            const maxY = Math.max(firstY, lastY);

            const listTmp = [];
            for (let x = minX; x <= maxX; x++) {
                for (let y = minY; y <= maxY; y++) {
                    listTmp.push(`${x}-${y}`);
                }
            }

            setCliquedSquares([...cliquedSquares, ...listTmp]);
            setSelectedSquares([]);
        }
    };


    const handleMouseEnter = (squareId: string) => {
        if (isMouseDown) {
            if (selectedSquares.includes(squareId)) {
                setSelectedSquares(selectedSquares.filter((id) => id !== squareId));
            } else {
                selectedSquares.push(squareId);
            }
            if (cliquedSquares.includes(squareId)) {
                setCliquedSquares(cliquedSquares.filter((id) => id !== squareId));
            } else {
                setCliquedSquares([...cliquedSquares, squareId]);
            }
        }
    };

    const generateGrid = (size: number) => {
        const newGrid = [];

        for (let i = 0; i < size; i++) {
            for (let j = 0; j < size; j++) {
                const squareId = `${i}-${j}`;
                newGrid.push(
                    <div
                        key={squareId}
                        id={squareId}
                        className={`square ${cliquedSquares.includes(squareId) ? "selected" : ""}`}
                        style={{
                            backgroundColor: selectedSquares.includes(squareId)
                                ? "#dbdae0"
                                : cliquedSquares.includes(squareId)
                                    ? "#10172A"
                                    : "#FFF",
                            border: "1px solid #dbdae0",
                            borderRadius: "15%",
                            width: "100%",
                            height: "100%",
                        }}
                        onClick={() => {
                            handleSquareClick(squareId);
                        }}
                        onMouseDown={() => {
                            handleMouseDown(squareId);
                        }}
                        onMouseUp={() => {
                            handleMouseUp();
                        }}
                        onMouseEnter={() => {
                            handleMouseEnter(squareId);
                        }}
                    ></div>
                );
            }
        }

        setGrid(newGrid);
    };

    useEffect(() => {
        generateGrid(gridSize);
    }, [gridSize, cliquedSquares, selectedSquares, isMouseDown]);

    const handleGridSizeChange = (value: number) => {
        setGridSize(value);
        setCliquedSquares([]);
    };

    const handleClearButtonClick = () => {
        setCliquedSquares([]);
    };


    const handleSubmitForm = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault()

    }


    return (
        <form onSubmit={handleSubmitForm}>
            <div
                className="grid-container"
                style={{
                    display: "grid",
                    gridTemplateColumns: `repeat(${gridSize}, 1fr)`,
                    gridTemplateRows: `repeat(${gridSize}, 1fr)`,
                    gap: "1%",
                    width: width,
                    height: height,
                }}
                onMouseLeave={() => {
                    handleMouseUp();
                }}
                onMouseUp={() => {
                    handleMouseUp();
                }}
            >
                {grid}
            </div>
            <div className={"flex justify-center items-center mt-5"}>
                <Button onClick={handleClearButtonClick}>Effacer</Button>
                <div className={"flex justify-center items-center ml-3"}>
                    <Select onValueChange={(value: string) => handleGridSizeChange(Number(value))}>
                        <SelectTrigger className="w-[180px] mr-5 h-12">
                            <SelectValue placeholder="Taille Grille" />
                        </SelectTrigger>
                        <SelectContent>
                            <SelectItem value="10">10x10</SelectItem>
                            <SelectItem value="15">15x15</SelectItem>
                            <SelectItem value="20">20x20</SelectItem>
                        </SelectContent>
                    </Select>
                </div>
            </div>
            <CardFooter className="flex justify-between">
                <Link href={"/builder/create-room"}><Button variant="secondary">Annuler</Button></Link>
                <Link href={{ pathname: '/builder/design-items-space', query: {name: name, superficie: superficie, matriceSalle: coordinatesListStr, tailleMatrice:gridSize}}}>
                    <Button>Suivant</Button>
                </Link>
            </CardFooter>
        </form>
    );
};

export default Grid;
