import { Metadata } from "next";
import Grid from "@/app/builder/design-room-space/components/grid";
import { Card, CardContent, CardDescription, CardHeader, CardTitle } from "@/registry/ui/card";

export const metadata: Metadata = {
    title: "Design Room Space",
    description: "Page in which the user will be able to design the space of his room.",
};


export default function DesignRoomSpace()
{
    return (
        <div className="flex justify-center items-center h-screen">
            <Card className="w-auto">
                <CardHeader>
                    <CardTitle className="flex justify-center items-center">Votre espace</CardTitle>
                    <CardDescription className="flex justify-center">Designez la forme de votre salle de restauration</CardDescription>
                </CardHeader>
                <CardContent className="flex justify-center flex-col items-center">
                    <Grid />
                </CardContent>

            </Card>
        </div>
    );
}
