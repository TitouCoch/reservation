'use client'

import React, { useState } from "react";
import { Label } from "@/registry/ui/label";
import { Input } from "@/registry/ui/input";
import { CardContent, CardFooter } from "@/registry/ui/card";
import { Button } from "@/registry/ui/button";
import {Slider} from "@/registry/ui/slider";
import Link from "next/link";

const CreateRoomForm = () => {
    const [formData, setFormData] = useState({
        name: "",
        superficie: 10,
    });
    const [superficieValue, setSuperficieValue] = useState<number[]>([10]); // Initialisez à 10 pour la valeur minimale.

    const handleInputChange = (e: { target: { name: any; value: any; }; }) => {
        const { name, value } = e.target;
        setFormData({
            ...formData,
            [name]: name === "superficie" ? parseInt(value, 10) : value,
        });
    };

    return (
        <>
            <CardContent>
                <form>
                    <div className="grid w-full items-center gap-4">
                        <div className="flex flex-col space-y-1.5">
                            <Label htmlFor="nom">Nom Espace</Label>
                            <Input
                                name="name"
                                placeholder="Terrasse"
                                value={formData.name}
                                onChange={handleInputChange}
                            />
                        </div>
                        <div className="flex flex-col space-y-1.5">
                            <Label htmlFor="superficie">Superficie (m²)</Label>
                            <Input
                                id="superficie"
                                value={superficieValue[0].toString()}
                                onChange={(e) => {
                                    const newValue = Number(e.target.value);
                                    if (newValue >= 10 && newValue <= 200) {
                                        setSuperficieValue([newValue]);
                                    }
                                }}
                            />
                            <div className={"flex justify-center"}>
                                <Slider
                                    style={{marginTop: "20px",marginBottom: "10px",width: "90%"}}
                                    value={superficieValue}
                                    onValueChange={(newValues) => setSuperficieValue(newValues)}
                                    max={200}
                                />
                            </div>

                        </div>
                    </div>
                    <Link href={{ pathname: '/builder/design-room-space', query: {name: formData.name, superficie: superficieValue[0]}}}>
                        <Button>Suivant</Button>
                    </Link>
                </form>
            </CardContent>
            <CardFooter className="flex justify-between">
                <Button variant="outline">Annuler</Button>
            </CardFooter>
        </>
    );
};

export { CreateRoomForm };
