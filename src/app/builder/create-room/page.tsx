import {Metadata} from "next";
import { Card, CardHeader, CardTitle,} from '@/registry/ui/card'
import {CreateRoomForm} from "@/app/builder/create-room/components/create-room-form";


export const metadata: Metadata = {
    title: "Create Room",
    description: "Page in which the user will be able to create his room.",
}

export default function CreateRoomPage() {
    return (
        <div className="flex justify-center items-center h-screen">
            <Card className="w-[350px]">
                <CardHeader>
                    <CardTitle className="flex justify-center items-center">Créer un Espace</CardTitle>
                </CardHeader>
                <CreateRoomForm/>
            </Card>
        </div>
    )
}