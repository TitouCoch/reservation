import React from "react";
import {Metadata} from "next";
import {Designer} from "@/app/builder/design-items-space/components/designer";
export const metadata: Metadata = {
    title: "Design Items Space",
    description: "Page in which the user will be able to design the items of his restaurant room.",
};

export default function DesignItemSpace() {

    return (
        <div className="flex justify-center items-center h-screen">
            <Designer/>
        </div>
    );
}


