'use client'

import React, {useId, useState} from "react";
import { CardContent } from "@/registry/ui/card";
import { useSearchParams } from "next/navigation";
import {DndContext, useDroppable, useDraggable} from "@dnd-kit/core";
import {Button} from "@/registry/ui/button";
import Link from "next/link";


const DraggableItem = () => {
    const { attributes, listeners, setNodeRef, transform } = useDraggable({
        id: "draggable-test",
    });

    const [position] = useState({ x: 0, y: 0 });

    const transformValue = transform ? `translate(${transform.x}px, ${transform.y}px)` : `translate(${position.x}px, ${position.y}px)`;
    return (
        <div
            ref={setNodeRef}
            {...attributes}
            {...listeners}
            style={{
                transform: transformValue,
                width: "30px",
                height: "30px",
                backgroundColor: "black",

            }}
        >
        </div>
    );
};

const DroppableArea = ({ value }: { value: string }) => {
    const [tmpValue, tmpRow, tmpCol] = value.split("-");

    const { isOver, setNodeRef } = useDroppable({
        id: `droppable-area-${tmpRow}-${tmpCol}`,
    });

    return (
        <div
            id={`droppable-area-${tmpRow}-${tmpCol}`}
            ref={setNodeRef}
            style={{
                width: "50px",
                height: "50px",
                border: tmpValue === "1" ? "1px solid gray" : "none",
                backgroundColor: isOver && tmpValue === "1" ? "red" : "white",
            }}
        ></div>
    );
};

const Designer = () => {
    const searchParams = useSearchParams();
    const coordinatesListStr: string | null = searchParams.get('matriceSalle');
    const gridSize: string | null = searchParams.get('tailleMatrice');
    const decodedCoordinatesList = coordinatesListStr != null ? JSON.parse(atob(coordinatesListStr)) : null;

    function convertListToMatrix(listStart: string[][], tailleMatrice: string | null) {
        if (tailleMatrice) {
            const convertTailleMatrice = Number(tailleMatrice);
            const listFinal = Array.from({ length: convertTailleMatrice }, () =>
                Array(convertTailleMatrice).fill("0")
            );

            for (const squareId of listStart) {
                const [xStr, yStr] = squareId;
                const x = Number(xStr);
                const y = Number(yStr);
                if (!isNaN(x) && !isNaN(y) && x < convertTailleMatrice && y < convertTailleMatrice) {
                    listFinal[x][y] = "1";
                }
            }
            return listFinal;
        }
    }

    const listFinal: any[][] | undefined = convertListToMatrix(decodedCoordinatesList, gridSize);
    const id = useId()

    return (
        <>
            <CardContent>
                <h1>Designer</h1>
                {listFinal ? (
                    <DndContext id={id}>
                        <div className="flex flex-col">
                            {listFinal.map((row, rowIndex) => (
                                <div key={rowIndex} className="flex">
                                    {row.map((value, colIndex) => (
                                        <DroppableArea
                                            key={`${rowIndex}-${colIndex}`}
                                            value={`${value}-${rowIndex}-${colIndex}`}
                                        />
                                    ))}
                                </div>
                            ))}
                        </div>
                        <DraggableItem/>
                    </DndContext>
                ) : (
                    <div className="flex w-full h-full flex-col items-center justify-center gap-4">
                        <h2 className="text-destructive text-4xl">Erreur!</h2>
                        <Button asChild>
                            <Link href={"/"}>Revenir au menu précédent</Link>
                        </Button>
                    </div>
                )}
            </CardContent>
        </>
    );
};

export { Designer };